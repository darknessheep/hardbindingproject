package View;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;


public class MainPage {

    @FXML
    Button buttonTest1;

    public void initialize(){

    }

    public void launchTest1() throws IOException {
        Stage stage = new Stage();
        URL url = getClass().getResource("/FXML/Test1Page.fxml");
        Parent test1Page = FXMLLoader.<Parent>load(url);
        Scene maTest1Page = new Scene(test1Page);

        stage.setScene(maTest1Page);
        stage.show();
    }
}
