package View;

import Modele.Test1.Dresseur;
import Modele.Test1.MyListCell;
import Modele.Test1.Pokemon;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.List;


public class Test1Page {

    private Dresseur monDresseur = new Dresseur();

    @FXML
    Button addToList;

    @FXML
    ListView<Pokemon> maListeView;

    @FXML
    Label monLabelNom;

    @FXML
    TextField monTfNom;

    @FXML
    public void initialize(){

        maListeView.itemsProperty().bind(monDresseur.equipeProperty());
        maListeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object old, Object nouveau) {
                if (old != null){
                    monTfNom.textProperty().unbindBidirectional(((Pokemon)old).nomProperty());
                }
                if (nouveau != null){
                    monLabelNom.textProperty().bind(((Pokemon)nouveau).nomProperty());
                    monTfNom.textProperty().bindBidirectional(((Pokemon)nouveau).nomProperty());
                }
            }
        });

        maListeView.setCellFactory(__ -> new MyListCell());
    }

    @FXML
    public void addToList(ActionEvent actionEvent) {
        monDresseur.addPokemon(new Pokemon("nom générique"));
    }
}
