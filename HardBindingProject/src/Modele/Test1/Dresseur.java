package Modele.Test1;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Dresseur {

    private ListProperty<Pokemon> equipe = new SimpleListProperty();
        public ListProperty equipeProperty () { return equipe;}
        public ObservableList getEquipe () { return equipe.getValue();}
        //public void setEquipe()

    public Dresseur (){
        equipe.set(FXCollections.observableArrayList());
    }

    public void addPokemon (Pokemon p){
        equipe.add(p);
    }

    public void suppPokemon (Pokemon p ){
        if (equipe.contains(p)){
            equipe.remove(p);
        }
    }
}
