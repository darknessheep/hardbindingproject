package Launch;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class Launch extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        URL url = getClass().getResource("/FXML/mainPage.fxml");
        Parent maMainPageFXML = FXMLLoader.<Parent>load(url);
        Scene maMainScene = new Scene(maMainPageFXML);

        stage.setScene(maMainScene);
        stage.show();
    }
}
